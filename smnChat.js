const path = require('path');
const http = require('http');
const express = require('express');
const socketio = require('socket.io');

const app = express();
const server = http.createServer(app);
const port = 3000 || process.env.port;
const io = socketio(server);

var chatContent = "";

app.use(express.static(path.join(__dirname, 'public')));


// Runs on Client Connect
io.on('connection', socket =>  {

    socket.emit('socketValidation', "user");
    var clientID = 'nil';
    socket.on('newClientID', inClientID => {
        clientID = inClientID;
        console.log("[New Client Connection: " + inClientID + "]");
    });

    socket.on('userMsg', usrMsgIn =>  {
        console.log(usrMsgIn);
        chatContent += usrMsgIn + "_nl_";
        io.emit('chatContent', chatContent);
    });

    socket.on('disconnect', () =>  {
        console.log("[Client Left Server]");
    });

    io.emit('chatContent', chatContent);
});


//Runs on server start
server.listen(port, () => {
    console.log("[Server Started]")
});

// Close Server
process.on('SIGTERM', () => {
    server.close(() => {
      console.log('Process terminated');
    });
});
