
const socket = io()
var userID = "user_"+Math.random().toString().substring(2, 10);
var userName = "Guest" + Math.random().toString().substring(2, 4);

//Functions

//Cookie Management
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; SameSite=Strict; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    document.cookie = name +'=; Path=/; SameSite=Strict; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}


//Main Script Content
console.log("[Client Connected with ID: " + userID + "]")
if(getCookie('userdata') != null){
    userName = getCookie('userdata').split('|')[0];
}

socket.on('socketValidation', clientValidation => {
    console.log("Connection Type: " + clientValidation);
    socket.emit('newClientID', userID+"|"+ userName);
});

socket.on('chatContent', updatedChat => {
    if(updatedChat != "")
        document.getElementById('chatcontent').innerHTML = updatedChat.replaceAll("_nl_", "<br>");
});

function sendMsg(msgIn, isServerMsg) {
    if(isServerMsg == false){
        socket.emit('userMsg', userName + ": " + msgIn);
    } else {
        socket.emit('userMsg', msgIn);
    }
}

function setUsername(usernameIn, showMsg) {
    if(showMsg) {
        sendMsg("_nl_["+userName+" changed their name to "  + usernameIn + ".]_nl_", true);
        userName = usernameIn;

        setCookie('userdata', userName + "|" + userID, 7);
    } else {
        userName = usernameIn;
    }
    
    
}
